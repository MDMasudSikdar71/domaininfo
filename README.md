# domain-info

This package contains a class that can fetch DNS records and whois info.

First make a folder and put donwloaded files there. Then Edit your composer file.

```
"autoload": {
        "psr-4": {
            "App\\": "app/",
            "MDMasudSikdar71\\DomainInfo\\": "YOUR_PROJECT_FOLDER_PATH/src/",
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/"
        }
    },
```

After that 

``composer dump-autoload``

## Example of usage

```php
<?php

namespace App\Http\Controllers;

use MDMasudSikdar71\DomainInfo\DomainInfo;

class ExampleController extends Controller
{
    public function exampleFunction()
    {
        $sld = 'systemdecoder.com';
        $domain = new DomainInfo($sld);
        $whois_answer = $domain->info();

        if ($domain->isAvailable()) {
            echo "Domain is available\n";
        } else {
            echo "Domain is registered\n";
        }

        echo $whois_answer;
    }
}


```

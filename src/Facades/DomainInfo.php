<?php

namespace MDMasudSikdar71\DomainInfo\Facades;

use Illuminate\Support\Facades\Facade;

class DomainInfo extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'domain_info'; }
}

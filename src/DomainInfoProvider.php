<?php

namespace MDMasudSikdar71\DomainInfo;

use Illuminate\Support\ServiceProvider;

class DomainInfoProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom('/config/domaininfo.php', 'domaininfo');

        $this->publishes([
            __DIR__ . '/config/domaininfo.php' => config_path('domaininfo.php')
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
